(()=>{
"use strict";
	const config = require( "json-cfg" ).load(`${__dirname}/config.json`);
	const http	 = require( 'http' );
	const sign 	 = require( 'jsrsasign' );
	const jwt	 = require( 'lib-jwt' )
	const objDefSyntax = new RegExp(/^{.*}$/);


	// US_REGISTER_KEY();

	// US_REGISTER_INFO("93109daf05e43e2123eca3b3e0df331c2138c64cbc6383b99dadf1c356343d30", "5ff033310c17d1fe846b775064569f37261e981e1c4d25fcb34ba5547fb83be3", "王笨蛋", "fishbitecat@gmail.com");

	QS_VALIDATE_QUERY("5ff033310c17d1fe846b775064569f37261e981e1c4d25fcb34ba5547fb83be3");

	function JWTParse(jwt){

		let parts = (jwt||'').split('.');
			if ( parts.length < 2 ) {
				console.log("1")
				return null;
			}



			let [header, payload, signature] = parts;
			try {
				console.log("3")
				header	= decode( header, 'utf8' );
				console.log("4")
				payload = decode( payload, 'utf8' );
				console.log("5")
				console.log(header)
				if ( !objDefSyntax.test(header) ) {
					console.log("9")
				}
				console.log(payload)
				if ( !objDefSyntax.test(payload) ) {
					console.log("10")
				}


				console.log("6")
				header		= JSON.parse( header );
				console.log("7")
				payload		= JSON.parse( payload );
				console.log("8")
				signature	= signature || '';
			}
			catch(e) {
				console.log("2")
				return null;
			}



			return {
				header,
				payload,
				raw:{
					content:`${parts[0]}.${parts[1]}`,
					signature
				}
			};

		function encode(input, encoding) {
			let buffer = Buffer.isBuffer(input) ? input : Buffer.from(input, encoding || 'utf8');
			return fromBase64(buffer.toString( "base64" ));
		}
		function decode(base64url, encoding) {
			let buffer = Buffer.from(toBase64(base64url), 'base64');
			return (arguments.length > 1) ? buffer.toString(encoding) : buffer;
		}
		function toBase64(base64url) {
			return (base64url + '==='.slice((base64url.length + 3) % 4))
				.replace(/\-/g, "+")
				.replace(/_/g, "/");
		}
		function fromBase64(base64) {
			return base64
				.replace(/=/g, "")
				.replace(/\+/g, "-")
				.replace(/\//g, "_");
		}
	}

	function CreateKeypair(){
		let ec = new sign.KJUR.crypto.ECDSA({"curve": "secp256k1"});
		let keypair = ec.generateKeyPairHex();
		let eccPrivateKey = keypair.ecprvhex;
		let eccPublicKey = keypair.ecpubhex;
		console.log(`私鑰${eccPrivateKey}`)
		console.log(`公鑰${eccPublicKey}`)
	}

	function QS_VALIDATE_QUERY(address){
		Promise.resolve()
		.then(()=>{
			let params = { address: address };
			let paramsJSON = JSON.stringify(params);
			RegisterToServer("ba.exp.hyper-ex.io", 80, "/qs/validate/query", "", paramsJSON);
		})
		.catch((err)=>{
			console.log(err);
		})
	}

	function US_REGISTER_KEY(){
		Promise.resolve()
		.then(()=>{
			console.log("產生一組新帳號");
			// 產生公私鑰匙, 使用secp256k1
			let ec = new sign.KJUR.crypto.ECDSA({"curve": "secp256k1"});
			let keypair = ec.generateKeyPairHex();
			let eccPrivateKey = keypair.ecprvhex;
			let eccPublicKey = keypair.ecpubhex;
			console.log(`私鑰 ${eccPrivateKey}`)
			console.log(`公鑰 ${eccPublicKey}`)

			let header = `
			{
				"type":"JWT",
				"alg":"SHA256withECDSA",
				"curve":"secp256k1"
			}
			`;

			let payload = `{
				"exp":1526457771434,
				"iss":"user",
				"key":"${eccPublicKey}"
			}`;

			return {header:header, payload:payload, key:eccPrivateKey};
		})
		.then((data)=>{
			// 完成jwt簽名
			let jwt = SHA256withECDSA(data.header, data.payload, data.key);
			console.log("完成jwt簽名");
			return jwt;
		})
		.then((jwt)=>{
			// 上傳jwt
			console.log("上傳");
			console.log(jwt);
			RegisterToServer("usr.exp.hyper-ex.io", "80", "/us/register/key", jwt);
		})
	}

	function US_REGISTER_INFO(priKey, address, name, email){
		return Promise.resolve()
		.then(()=>{
			// header
			let header = `
			{
				"type":"JWT",
				"alg":"SHA256withECDSA",
				"curve":"secp256k1"				
			}
			`;

			// register info
			let payload = `{
				"exp":1526457771434,
				"iss":"user",
				"address":"${address}",
				"name":"${name}",
				"email":"${email}"
			}`;

			return {header:header, payload:payload, key:priKey};
		})
		.then((data)=>{
			// 完成jwt簽名
			let jwt = SHA256withECDSA(data.header, data.payload, data.key);
			console.log("完成jwt簽名");
			// console.log(jwt);
			return jwt;
		})
		.then((jwt)=>{
			// 上傳jwt
			console.log("上傳");
			console.log(jwt);
			RegisterToServer("usr.exp.hyper-ex.io", 80, "/us/register/info", jwt);
		})
	}


	function SHA256withECDSA(header, payload, key){
		// header
		header = _CleanString(header);
		header = Buffer.from(header).toString('base64');
		header = _ToBase64URL(header);
		// console.log("--header---------")
		// console.log(header)

		// payload
		payload = _CleanString(payload);
		payload = Buffer.from(payload).toString('base64');
		payload = _ToBase64URL(payload);
		// console.log("--payload---------")
		// console.log(payload)

		// pure SHA256
		// let data = `${header}.${payload}`;
		// let shaObj = new jsSHA("SHA-256", "TEXT");
		// shaObj.update(data);
		// let jwt = shaObj.getHash("B64");
		// jwt = _ToBase64URL(jwt);

		// sign
		let data = `${header}.${payload}`;
		// console.log("--data---------")
		// console.log(data)
		let sig = new sign.KJUR.crypto.Signature({"alg": "SHA256withECDSA"});
		// console.log("--sig---------")
		// console.log(sig)
		sig.init({d: key, curve: "secp256k1"});
		sig.updateString(data);
		let signHexStr = sig.sign();
		let jwtStr = "";
		// console.log("--signHexStr---------")
		// console.log(signHexStr)

		jwtStr = `${data}.${signHexStr}`;

		return jwtStr;


		function _FromBase64(base64url) {
			return (base64url + '==='.slice((base64url.length + 3) % 4))
				.replace(/\-/g, "+")
				.replace(/_/g, "/");
		}

		function _ToBase64URL(base64) {
			return String(base64)
				.replace(/=/g, "")
				.replace(/\+/g, "-")
				.replace(/\//g, "_");
		}

		function _CleanString(string){
			return string.replace(/\t/g, "").replace(/\s/g, "").replace(/\n/g, "");
		}

		function _Hex2bin(hex){
			return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8);
		}

		function _b64EncodeUnicode(str) {
			// first we use encodeURIComponent to get percent-encoded UTF-8,
			// then we convert the percent encodings into raw bytes which
			// can be fed into btoa.
			return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
				function toSolidBytes(match, p1) {
					return String.fromCharCode('0x' + p1);
			}));
		}
	};
	function RegisterToServer(host, port, api, jwt="", data="{}"){

		var options = {
			host: `${host}`,
			path: `${api}`,
			port: `${port}`,
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				jwt: jwt
			}
		};

		var callback = function(response) {
			var str = '';

			//another chunk of data has been recieved, so append it to `str`
			response.on('data', function (chunk) {
				str += chunk;
			});

			//the whole response has been recieved, so we just print it out here
			response.on('end', function () {
				console.log(str);
			});
		}

		let req = http.request(options, callback).on('error', function(err) {
			console.log('error ' + err);
		});
		req.write(data);
		req.end();
	}
})();